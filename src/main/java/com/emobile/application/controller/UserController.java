package com.emobile.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.emobile.application.dto.UserRequestResponseDto;
import com.emobile.application.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/admin")
@Api(value = "User Resource Endpoint.", description = "")
public class UserController {

	@Autowired
	UserService userService;
	
	
	@ApiOperation(value = "fetchRequests")
	@ApiResponses(
			value = {
					@ApiResponse(code = 404, message = "Not Found")
			}
	)
	
	/**
	 * @param message
	 * @return
	 */
	@GetMapping("/status")
	public ResponseEntity<List<UserRequestResponseDto>> fetchRequests(@RequestParam String message){
		return new ResponseEntity<List<UserRequestResponseDto>>(userService.getUserRequests(message),HttpStatus.OK);
		
	}
}
