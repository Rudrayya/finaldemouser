package com.emobile.application.dto;
 class UserRequestDto {

	private Integer requestId;
	private String userName;
	private String userEmail;
	private String panNo;
	private Integer planId;
	

	public UserRequestDto() {
	}

	public UserRequestDto(Integer requestId, String userName, String userEmail, String panNo, Integer planId,
			String requestStatus) {
		super();
		this.requestId = requestId;
		this.userName = userName;
		this.userEmail = userEmail;
		this.panNo = panNo;
		this.planId = planId;
		
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	

}
