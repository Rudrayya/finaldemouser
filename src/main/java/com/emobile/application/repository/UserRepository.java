package com.emobile.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emobile.application.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
