package com.emobile.application.constant;

public interface UserConstant {

	String PLACENEWSIM = "Place the request for new simcard";
	String PLANNOTFOUND ="Selected Plan is not available Now";
	String INSTATUS ="inprogress";
	String REQUESTMESSAGE ="Request is Placed Successfully";
	String REQUSRIDNOTFOUND ="Given Request id not valid please give valid request Id";
}
